#! /bin/bash

path=${1:-/sharedfolders/chocolatey.org}
#echo path=$path

if [ ! -d $path ]
then
	echo "Bad path, exiting"
	exit 1
fi

#add logging here

#add dep check

find $path -name *.nupkg | while read file
do
dir=$(dirname "$file")
#echo $dir
if [[ -f $dir/downloaded ]]
then
    echo already downloaded $file
	continue
fi
if [[ -f $dir/nourl ]]
then
    echo no need to download $file
	continue
fi
echo $file >> $path/_logs/nupkgFiles.txt
done

gawk -i inplace '!a[$0]++' $path/_logs/nupkgFiles.txt