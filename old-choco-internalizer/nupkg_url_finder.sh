#! /bin/bash

path=${1:-/sharedfolders/chocolatey.org}

if [ ! -d $path ]
then
	echo "Bad path, exiting"
	exit 1
fi

#add logging here

#add dep check
#needs bash greater then v4? mapfile
#needs unzip
#needs gawk
#needs echo? checkme
#needs sed
#needs grep
#needs dirname

mapfile nupkgFiles < "$path/_logs/nupkgFiles.txt"
echo ${nupkgFiles[*]}

for nupkg in "${nupkgFiles[@]}"
do
	#echo $nupkg
	dir=$(dirname "${nupkg}")
	#add check for no urls available
	if ! unzip -p $nupkg tools/chocolateyinstall.ps1 &> /dev/null; then
		#echo No chocolateyinstall found for $nupkg
		touch $dir/nourl
	else
		#echo trying to unzip $nupkg
		mapfile -t urls < <(unzip -p $nupkg tools/chocolateyinstall.ps1 | grep -o -i \'http.*\') #>> $path/_logs/nupkgUrls.txt
		#echo ${urls[@]}
		
		if [[ ! ${urls[@]} ]]
		then 
		#echo $nupkg has no urls
		touch $dir/nourl
		else
		#echo saving $nupkg url #${urls[@]} >> $path/_logs/nupkgUrls.txt
		echo "aria2c -d $dir --auto-file-renaming=false ${urls[@]}" >> $path/_logs/nupkgUrls.txt
		fi
	fi
	sed -i '1d' "$path/_logs/nupkgFiles.txt"
done

gawk -i inplace '!a[$0]++' $path/_logs/nupkgUrls.txt