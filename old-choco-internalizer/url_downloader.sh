#! /bin/bash

path=${1:-/sharedfolders/chocolatey.org}

if [ ! -d $path ]
then
	echo "Bad path, exiting"
	exit 1
fi

#add logging

#add dep check

mapfile urls < "$path/_logs/nupkgUrls.txt"
echo 

for url in "${urls[@]}"
do
	#echo $url
	eval $url
	if [ $? -eq 0 ] 
	then
		#echo $url | grep -o -i ${path}.*\-\-a | hea
		folder="$(echo $url | grep -o -i ${path}.*\-\-a )"
		#echo $folder
		folder="${folder%????}"
		#echo $folder
		touch $folder/downloaded
		sed -i '1d' "$path/_logs/nupkgUrls.txt"
	else
		echo download failure $url
		sed '1d' "$path/_logs/nupkgUrls.txt" >> "$path/_logs/failedUrls.txt"
		sed -i '1d' "$path/_logs/nupkgUrls.txt"
	fi
done