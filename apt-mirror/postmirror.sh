#! /bin/bash

######## info ########
#Rev 1.0
#Updated 4/21/18 with 175.6gb total download in apt-mirror
#For Debian 9(Stretch)amd64 and apt-mirror
#By TheCakeIsNaOH

#Requirements are apt-mirror, apache2, dpkg-deb, and enought drive space for what repos you want to clone
#may work on apt distros that are not debian 9 amd64 with changes to the repo list

#The reason that OMV and Backports have seperate repos is that I do not want those packages available on all my debian systems, but only on seleceted ones

#At the bottom of the script is a list of repos used in mirror.list
######## end ########



######## Useage details ########
#In /etc/apt/mirror.list-
#base path is $filepath/apt-mirror
#mirrorpath is $base_path/mirror
#defaultarch is amd64(default on amd64 systems)
#postmirror_script is wherever you have this script
#run_postmirror is 1


#Set sources.list Debian amd64 clients on lan to-    deb [trusted=yes] http://<server lan IP>/repo ./
#If you want Open Media Vault add another line with- deb [trusted=yes] http://<server lan IP>/omv ./
#If you want Stretch-Backport add another line with- deb [trusted=yes] http://<server lan IP>/bpo9 ./


#$filepath/apt-custom is the place to locate any self-built packages or packages not available in a repository format online


#set filepath to what you have the apt-mirror base_path dir set to
filepath=/mnt/1tb
#the script expects repo, omv, bpo9, and apt-custom in the dir filepath is set to

#set htmlpath to what you where you want the repos to be located on you web server
htmlpath=/var/www/html
#the script expects dirs named repo, omv, and bpo9 locaed located in htmlpath/
######## end ########


####### script #######
#unlink apache2 pages to disable the repos
unlink $htmlpath/repo
unlink $htmlpath/omv
unlink $htmlpath/bpo9


# Clean out lan repo's in prep for updates
rm $filepath/repo/*
rm $filepath/omv/*
rm $filepath/bpo9/*


#find and hardlink all .deb files in directories to repo
find $filepath/apt-mirror/mirrors/deb.debian.org/ -name "*.deb" -exec ln {} $filepath/repo \;
find $filepath/apt-mirror/mirrors/security.debian.org/ -name "*.deb" -exec ln {} $filepath/repo \;
find $filepath/apt-mirror/mirrors/www.deb-multimedia.org/ -name "*.deb" -exec ln {} $filepath/repo \;
find $filepath/apt-mirror/mirrors/repo.daniil.it/ -name "*.deb" -exec ln {} $filepath/repo \;
find $filepath/apt-mirror/mirrors/dl.winehq.org/ -name "*.deb" -exec ln {} $filepath/repo \;
find $filepath/apt-mirror/mirrors/download.virtualbox.org/ -name "*.deb" -exec ln {} $filepath/repo \;
find $filepath/apt-mirror/mirrors/dl.google.com/ -name "*.deb" -exec ln {} $filepath/repo \;
find $filepath/apt-mirror/mirrors/linux.teamviewer.com/ -name "*.deb" -exec ln {} $filepath/repo \;
find $filepath/apt-custom/ -name "*.deb" -exec ln {} $filepath/repo \;


#find and hardlink all .deb files in directories to omv
find $filepath/apt-mirror/mirrors/packages.openmediavault.org/ -name "*.deb" -exec ln {} $filepath/omv \;


#find and hardlink all .deb files in directories to bpo9
find $filepath/apt-mirror/mirrors/ftp.debian.org/ -name "*.deb" -exec ln {} $filepath/bpo9 \;


#create new Packages.gz files
dpkg-scanpackages -m $filepath/repo/ | gzip -9c > $filepath/repo/Packages.gz
dpkg-scanpackages -m $filepath/omv/ | gzip -9c > $filepath/omv/Packages.gz
dpkg-scanpackages -m $filepath/bpo9/ | gzip -9c > $filepath/bpo9/Packages.gz

#relink(soft) repo dirs to apache2 server
ln -s $filepath/repo $htmlpath/repo
ln -s $filepath/omv $htmlpath/omv
ln -s $filepath/bpo9 $htmlpath/bpo9


#Offical repos
#deb-i386  http://deb.debian.org/debian stretch main contrib non-free
#deb-amd64 http://deb.debian.org/debian stretch main contrib non-free
#deb-src   http://deb.debian.org/debian stretch main contrib non-free

#deb-i386  http://deb.debian.org/debian stretch-updates main contrib non-free
#deb-amd64 http://deb.debian.org/debian stretch-updates main contrib non-free
#deb-src   http://deb.debian.org/debian stretch-updates main contrib non-free

#deb-i386  http://security.debian.org/debian-security/ stretch/updates main contrib non-free
#deb-amd64 http://security.debian.org/debian-security/ stretch/updates main contrib non-free
#deb-src   http://security.debian.org/debian-security/ stretch/updates main contrib non-free

#deb-i386  http://ftp.debian.org/debian stretch-backports main
#deb-amd64 http://ftp.debian.org/debian stretch-backports main
#deb-src   http://ftp.debian.org/debian stretch-backports main


#3rd-Party repos
#deb-i386  https://www.deb-multimedia.org/ stable main non-free
#deb-amd64 https://www.deb-multimedia.org/ stable main non-free

#yt video downloader
#deb       http://repo.daniil.it lenny main

#deb-i386  https://dl.winehq.org/wine-builds/debian/ stretch main

#deb-i386  http://download.virtualbox.org/virtualbox/debian stretch contrib
#deb-amd64 http://download.virtualbox.org/virtualbox/debian stretch contrib

#deb-amd64 http://dl.google.com/linux/chrome/deb/ stable main

#deb-amd64 http://linux.teamviewer.com/deb stable main
#deb-amd64 http://linux.teamviewer.com/deb preview main
#deb-amd64 http://linux.teamviewer.com/deb stable tv13
#deb-amd64 http://linux.teamviewer.com/deb preview tv13

#deb       http://packages.openmediavault.org/public arrakis main partner
#deb       http://packages.openmediavault.org/public arrakis-proposed main