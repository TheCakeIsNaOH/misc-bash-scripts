#!/bin/bash

# Locate the history file in your profile, and copy it to the same folder as this script.
# On Mac: ~/Library/Application\ Support/Google/Chrome/Default/History
# On Windows: C:\Users\YOUR USER NAME\AppData\Local\Google\Chrome\User Data\Default\History

cd ~/history

ctime=$(date "+%Y.%m.%d-%H.%M.%S")
cdate=$(date "+%Y.%m.%d")

cp /home/user/.config/google-chrome/Default/History ~/history/chrome-history

sqlite3 -header -csv chrome-history "select *, \
        datetime(last_visit_time / 1000000 + (strftime('%s', '1601-01-01T05:30:00')), 'unixepoch') \
        as visit_time from urls ORDER BY last_visit_time DESC;" > chrome-history-$cdate.csv

#mutt -s "Chrome History $cdate" -b email@gmail.com  -a ~/history/chrome-history-$cdate.csv -- email@gmail.com < ~/history/message.txt
#mutt -s "Chrome History $cdate" -a ~/history/chrome-history-$cdate.csv -- email@gmail.com < ~/history/message.txt
